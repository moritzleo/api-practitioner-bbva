var movimientosJSON = require('./movimientosv2.json');
var usuariosJSON = require('./usuarios.json');

var express = require('express');
var bodyparser = require('body-parser');
var requestJson = require('request-json');
//var jsonQuery = require('json-query');

var app = express();
app.use(bodyparser.json());

/*----Start sections for moves --*/

app.get('/', function(req, res){
  res.send('Hola API');
});

app.get('/v1/movimientos', function(req, res){
  res.sendfile('movimientosv1.json');
});

app.get('/v2/movimientos', function(req, res){
  res.send(movimientosJSON);
});

app.get('/v2/movimientos/:id', function(req, res){
  var idMovimiento = req.params.id;
  res.send(movimientosJSON[idMovimiento-1]);
});

app.put('/v2/movimientos/:id', function(req, res){
  var update = req.body;
  var idMovimiento = req.params.id;
  var actual = movimientosJSON[idMovimiento-1];

  if(update.importe != undefined){
    actual.importe = update.importe;
  }
  if(update.ciudad != undefined){
    actual.ciudad = update.ciudad;
  }
  //movimientosJSON[idMovimiento-1] = actual;
  res.send(movimientosJSON[idMovimiento-1]);
});

app.delete('/v2/movimientos/:id', function(req, res){
  var idMovimiento = req.params.id;
  var actual = movimientosJSON[idMovimiento-1];
  movimientosJSON.push({
    'id' : movimientosJSON.length+1,
    'ciudad' : actual.ciudad,
    'importe' : actual.importe * -1,
    'mensaje' : 'Movimiento Elimina'+idMovimiento
  });

  //movimientosJSON[idMovimiento-1] = actual;
  res.send('Se elimino el movimiento');
});

app.post('/v2/movimientos',function(req, res){
  var nuevo = req.body;
  nuevo.id = movimientosJSON.length + 1;
  movimientosJSON.push(req.body);
  res.send(nuevo);
});

app.post('/v2/movimientosq',function(req, res){
  console.log(req);
  console.log(req.headers['authorization']);
  if(req.headers['authorization'] != undefined){
    res.send('Recibido');
  }else{
    res.send('Error');
  }
});

/*--- End moves --*/

/*--- Start sections users --*/
app.get('/v1/usuarios/:id', function(req, res){
  var idUser = req.params.id;
  res.send(usuariosJSON[idUser-1]);
});

app.post('/v1/usuarios/login',function(req, res){
  var useremail = req.headers['e'];
  var usepass = req.headers['p'];

  /*var resultados = jsonQuery('[email=' + useremail + ' ]',{data:usuariosJSON});

  if(resultados.value != null && resultados.value.password == usepass){
    usuariosJSON[i].status = 1;
    res.send('Autenticado');
  }*/

  for(var i = 0 ; i < usuariosJSON.length ; i++){
    if(usuariosJSON[i].email == useremail && usuariosJSON[i].pass == usepass && usuariosJSON[i].status != 1){
      usuariosJSON[i].status = 1;
      res.send('Autenticado');
    }
  }
  res.send('Error de autenticacón');
});

app.put('/v1/usuarios/:id', function(req, res){
  var update = req.body;
  var idUsuario = req.params.id;
  var actual = usuariosJSON[idUsuario-1];

  if(update.status == 0 && actual.status == 1){
    actual.status = update.status;
    res.send('{"user" : ' + actual.nombre + ', status : ' + update.status +'}');
  }else{
    res.send('Este usuarios no tiene sesion');
  }
});

/*--- End users --*/

/*Version 3 API conectando mLab*/
var urlMLabRaiz = 'https://api.mlab.com/api/1/databases/techumx/collections';
var apiKey = 'apiKey=ttm7XjSeGR3u6LVLmSB8HOYQpUJBtNrz';
var clienteMLab = requestJson.createClient(urlMLabRaiz+'?'+apiKey);

app.get('/v3', function(req,res){
  clienteMLab.get('', function(err, resM, body){
    var coleccionesUsuario = [];
    if(!err){
      for (var i = 0; i < body.length; i++) {
        if( body[i] != "system.indexes"){
          coleccionesUsuario.push({'recurso' : body[i], 'url' : '/v3/'+body[i]});
        }
      }
      res.send(coleccionesUsuario);
    }else{
      res.send(err);
    }

  })
});

app.get('/v3/usuarios',function(req, res){
  clienteMLab = requestJson.createClient(urlMLabRaiz+"/usuarios?"+apiKey);
  clienteMLab.get('',function(err, resM, body){
     res.send(body);
  })
})

app.post('/v3/usuarios',function(req, res){
  clienteMLab = requestJson.createClient(urlMLabRaiz+"/usuarios?"+apiKey);
  clienteMLab.post('',req.body,function(err, resM, body){
     res.send(body);
  })
});

app.get('/v3/usuarios/:id',function(req, res){
  clienteMLab = requestJson.createClient(urlMLabRaiz+"/usuarios");
  clienteMLab.get('?q={"idusuario" : '+req.params.id+'}&'+apiKey,function(err, resM, body){
     res.send(body);
  })
});

app.put('/v3/usuarios/:id',function(req, res){
  var update = req.body;
  var idUsuario = req.params.id;
  clienteMLab = requestJson.createClient(urlMLabRaiz+"/usuarios");
  clienteMLab.put('?q={"idusuario" : '+req.params.id+'}&'+apiKey, { "$set" : update } ,function(err, resM, body){
     res.send(body);
  })
});

app.get('/v3/usuarios',function(req, res){
  clienteMLab = requestJson.createClient(urlMLabRaiz+"/usuarios?"+apiKey);
  clienteMLab.get('',function(err, resM, body){
     res.send(body);
  })
})

/*movimientos*/
app.get('/v3/movimientos',function(req, res){
  clienteMLab = requestJson.createClient(urlMLabRaiz+"/movimientos?"+apiKey);
  clienteMLab.get('',function(err, resM, body){
     res.send(body);
  })
});

app.get('/v3/movimientos/:id',function(req, res){
  clienteMLab = requestJson.createClient(urlMLabRaiz+"/movimientos");
  clienteMLab.get('?q={"idcuenta" : "'+req.params.id+'"}&'+apiKey,function(err, resM, body){
     res.send(body);
  })
});

/*PDF download*/
app.get('/v4', function(req, res){
  res.sendfile('./files/guia.pdf');
});

/*Cuentas*/
app.get('/v5/cuentas/:id',function(req, res){
  clienteMLab = requestJson.createClient(urlMLabRaiz+"/cuentas");
  clienteMLab.get('?q={"idCuenta" : '+req.params.id+'}&f={"movimientos":1}&'+apiKey,function(err, resM, body){
     res.send(body);
  })
});

app.get('/v5/cuentas/usuario/:id',function(req, res){
  clienteMLab = requestJson.createClient(urlMLabRaiz+"/cuentas");
  //  ?q={"usuario":"USR000001"}&f={"idcuenta":1}
  clienteMLab.get('?q={"usuario" : "'+req.params.id+'"}&'+apiKey,function(err, resM, body){
     res.send(body);
  })
});

app.get('/v5/cuentas/:id/:idMov',function(req, res){
  clienteMLab = requestJson.createClient(urlMLabRaiz+"/cuentas");
  clienteMLab.get('?q={"idCuenta" : '+req.params.id+'}&f={"movimientos":1}&s={"idmov": '+req.params.idMov+' }&'+apiKey,function(err, resM, body){
     res.send(body);
  })
});

app.post('/v5/usuarios',function(req, res){
  clienteMLab = requestJson.createClient(urlMLabRaiz+"/usuarios?"+apiKey);
  clienteMLab.post('',req.body,function(err, resM, body){
     res.send(body);
  })
});

app.listen(3000);

console.log("escuchando en el puerto 3000");
