var mocha = require("mocha");
var chai = require("chai");
var chaiHttp = require("chai-http");
var server = require('../server');

var should = chai.should();

chai.use(chaiHttp);

describe('test de actividad',() => {
  it("Google functions",(done) => {
    chai.request('http://www.google.com.mx')
    .get('/')
    .end((err, res) => {
      done();
    });
  });
});


describe('Test API',() => {

  it("Raiz API",(done) => {
    chai.request('http://localhost:3000')
    .get('/v3')
    .end((err, res) => {
      //console.log(res.body)
      res.should.have.status(200);
      done();
    });
  });

  it("Raiz API array",(done) => {
    chai.request('http://localhost:3000')
    .get('/v3')
    .end((err, res) => {
      //console.log(res.body)
      res.should.have.status(200);
      res.body.should.be.a('array');
      done();
    });
  });

  it("Raiz API devuelve dos elementos",(done) => {
    chai.request('http://localhost:3000')
    .get('/v3')
    .end((err, res) => {
      //console.log(res.body)
      res.should.have.status(200);
      res.body.should.be.a('array');
      res.body.length.should.be.eql(2);
      done();
    });
  });

  it("Raiz API devuelve objetos correctos",(done) => {
    chai.request('http://localhost:3000')
    .get('/v3')
    .end((err, res) => {
      //console.log(res.body)
      res.should.have.status(200);
      res.body.should.be.a('array');
      res.body.length.should.be.eql(2);
      for (var i = 0; i < res.body.length; i++) {
        res.body[i].should.have.property('recurso');
        res.body[i].should.have.property('url');
      }
      done();
    });
  });
});

describe('Test API movimientos',() => {

  it("Raiz de API movimientos contesta",(done) => {
    chai.request('http://localhost:3000')
    .get('/v3/movimientos')
    .end((err, res) => {
      //console.log(res.body)
      res.should.have.status(200);
      done();
    });
  });

  it("Raiz de API movimientos es array",(done) => {
    chai.request('http://localhost:3000')
    .get('/v3/movimientos')
    .end((err, res) => {
      //console.log(res.body)
      res.should.have.status(200);
      res.body.should.be.a('array');
      done();
    });
  });

  it("Raiz de API movimiento existe",(done) => {
    chai.request('http://localhost:3000')
    .get('/v3/movimientos/MD73 ACVI ZZMZ NHIV LSYZ RZEQ')
    .end((err, res) => {
      //console.log(res.body)
      res.should.have.status(200);
      done();
    });
  });

  it("Raiz de API movimiento contesta y es array",(done) => {
    chai.request('http://localhost:3000')
    .get('/v3/movimientos/MD73 ACVI ZZMZ NHIV LSYZ RZEQ')
    .end((err, res) => {
      //console.log(res.body)
      res.should.have.status(200);
      res.body.should.be.a('array');
      done();
    });
  });

  it("Raiz de API movimiento contesta y devuelve el correcto",(done) => {
    chai.request('http://localhost:3000')
    .get('/v3/movimientos/MD73 ACVI ZZMZ NHIV LSYZ RZEQ')
    .end((err, res) => {
      //console.log(res.body)
      res.should.have.status(200);
      res.body.should.be.a('array');
      res.body[0].should.have.property('idcuenta').which.is.eql('MD73 ACVI ZZMZ NHIV LSYZ RZEQ');
      done();
    });
  });

});
